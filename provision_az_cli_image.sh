echo "install kubectl"
az aks install-cli
echo "az login"
az login --service-principal \
    -u $AZ_SP_ID \
    -p $AZ_SP_PASSWORD \
    --tenant $AZ_SP_TENANT_ID
echo "link kubectl with aks"
az aks get-credentials --resource-group Modanisa-ToDo-app --name aks-todo
echo "install helm"
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh