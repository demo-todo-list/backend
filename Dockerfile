FROM golang:alpine AS compile-image

ENV CGO_ENABLED=0
ENV GOOS=linux

WORKDIR /app

COPY ./go.* ./
RUN go mod download
COPY . ./
RUN go build -o goapp
# COPY ./backend-app/go.* ./
# RUN go mod download
# COPY ./backend-app ./backend-app
# RUN cd backend-app/ && go build -o goapp

FROM alpine AS build-image

RUN apk --no-cache add ca-certificates

# WORKDIR /app/backend-app

WORKDIR /app

# COPY --from=compile-image /etc/passwd /etc/passwd
# COPY --from=compile-image /etc/group /etc/group

RUN apk update && apk add --no-cache git
RUN addgroup -S appgroup && adduser -S appuser -G appgroup --disabled-password
USER appuser:appgroup
COPY ./.config ./.config
COPY --from=compile-image /app/goapp /app/
EXPOSE 80
ENTRYPOINT ["./goapp"]