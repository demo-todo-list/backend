# Todo App Project Backend

Backend Api for a Web Based ToDo List Application.

## Requirements

[go](https://golang.org/doc/install)

[pact](https://github.com/pact-foundation/jest-pact)

## Install, Build And Tests

### Compile development

```bash
go build
```

### Lints and fixes files

```bash
golangci-lint run -c .config/.golangci.yaml -v
```

### Test Process

#### Run the tests

```bash
go test ./... (For all tests)
```

#### Run the provider test

```bash
CGO_ENABLED=0 go test -v test/pact/provider_test.go
```

## Directory Structure

```sh
.
├── .config
│   ├── dev.yaml                    - Port and appname conf
│   └── prod.yaml                   - Port and appname conf
├── config
│   ├── config.go
│   └── config_test.go              - Config test with TDD
├── docker-compose.yml              - Docker-compose file to up backend and MySQL containers (for dev purposes).
├── Dockerfile
├── .dockerignore
├── .env
├── .gitignore
├── .gitlab-ci.yml                  - Used for gitlab pipeline configurations.
├── go.mod                          - Go dependencies file
├── go.sum                          - Go dependencies file
├── handler
│   ├── todo_handler.go
│   └── todo_handler_test.go        - Handler test with TDD
├── main.go
├── .mocks
│   ├── mock_todo_repository.go     - Generated file for mock repository.     
│   └── mock_todo_service.go        - Generated file for mock service.   
├── model
│   └── todo_model.go
├── provision_az_cli_image.sh       - Bash script that provision the CD stage in the pipeline
├── README.md
├── repository
│   └── todo_repository.go
├── server
│   └── server.go
├── service
│   ├── todo_service.go
│   └── todo_service_test.go        - Service test with TDD
├── terraform-aks-k8s
│   ├── k8s.tf                      - Terraform configuration file that declares the resources for the Kubernetes cluster.
│   ├── main.tf                     - Terraform configuration file that declares the Azure provider.
│   ├── output.tf                   - Terraform configuration file that allows access to the cluster with kubectl.
│   └── variables.tf                - Terraform configuration file that declares variables for the main.tf.
├── test
│   ├── pact
│   │   ├── log                     - This file has provider test logs
│   │   └── provider_test.go        - This file is using for provider test
│   └── testdata
│       └── test-config.yaml        - This file is using for config port test 
└── todo-backend
    ├── Chart.yaml
    ├── .helmignore
    ├── mysql-values.yml            - Overrides mysql helm values
    ├── templates
    │   ├── deployment.yml          - Kubernetes deployment yaml
    │   ├── secrets.yml             - Kubernetes secret yaml
    │   └── service.yaml            - Kubernetes service yaml
    └── values.yaml                 - Helm values
```

## Deployment Process

On Gitlab Pipeline

- build
- lint
- test
- pact-publish
- package
- deploy

## Parameters

The following tables lists the configurable parameters of the Backend chart and their default values.

|Parameter | Description | Default |
| ------ | ------ | ------ |
| app.name | Deployment name | todo-backend |
| app.group | Backend service metadata label | todo-backend |
| app.replicaCount | Number of Backend replicas | 1 |
| app.image.name | Backend image name | "registry.gitlab.com/demo-todo-list/backend" |
| app.image.tag | Backend image tag | develop |
| app.image.pullPolicy | Backend image pull policy | Always |
| app.container.port | Backend pod port | 3000 |
| app.container.env | List of additional Environment Variables as key/value dictionary | [] |
| app.service.type | Backend service type | LoadBalancer |
| app.service.port | Backend service port | 3000 |

### To deploy using helm manually

```bash
helm registry login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
helm chart pull $CI_REGISTRY/$CI_PROJECT_PATH:helm-$CI_BUILD_REF_NAME
helm chart export $CI_REGISTRY/$CI_PROJECT_PATH:helm-$CI_BUILD_REF_NAME -d .
```

## Production URL

<http://104.45.65.172:3000/getTodoList>
